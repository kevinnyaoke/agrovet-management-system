@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">View products <ul class="nav nav-pills nav-justified">
                        <li><a class="btn btn-info" href="{{route ('create')}}">Create</a></li>
                        <li class="active"><a class="btn btn-info" href="{{route ('view')}}">view products</a>
                        </li>
                    </ul>
                </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <div>
                        <table class="table table-hover" id="myTable">
                            <thead>
                                <tr>
                                    <th>Category</th>
                                    <th>Name</th>
                                    <th>Price</th>
                                    <th>Description</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody id="">

                                <tr>

                                    <td>Feeds</td>
                                    <td>nappier grass</td>
                                    <td>1500</td>
                                    <td>Description</td>
                                </tr>
                                <tr>

                                    <td>Drugs</td>
                                    <td>Deworming</td>
                                    <td>2000</td>
                                    <td>Description</td>
                                </tr>
                                <tr>

                                    <td>Drugs</td>
                                    <td>Deworming</td>
                                    <td>2000</td>
                                    <td>Description</td>
                                </tr>
                                <tr>

                                    <td>Drugs</td>
                                    <td>Deworming</td>
                                    <td>2000</td>
                                    <td>Description</td>
                                </tr>
                                <tr>

                                    <td>Drugs</td>
                                    <td>Deworming</td>
                                    <td>2000</td>
                                    <td>Description</td>
                                </tr>
                                <tr>

                                    <td>Drugs</td>
                                    <td>Deworming</td>
                                    <td>2000</td>
                                    <td>Description</td>
                                </tr>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection