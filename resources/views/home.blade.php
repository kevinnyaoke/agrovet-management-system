@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard <ul class="nav nav-pills nav-justified">
                        <li><a class="btn btn-info" href="{{route ('create')}}">Create</a></li>
                        <li class="active"><a class="btn btn-info" href="{{route ('view')}}">view products</a>
                        </li>
                    </ul>
                </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif


                </div>
            </div>
        </div>
    </div>
</div>
@endsection