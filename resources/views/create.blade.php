@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create <ul class="nav nav-pills nav-justified">
                        <li><a class="btn btn-info" href="{{route ('create')}}">Create</a></li>
                        <li class="active"><a class="btn btn-info" href="{{route ('view')}}">view products</a>
                        </li>
                    </ul>
                </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif


                    <form role="form" method="POST" action="">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="name">Category:</label>
                            <input type="text" class="form-control" id="category" name="category" placeholder="category" required>
                        </div>
                        <div class="form-group">
                            <label for="quantity">Name:</label>
                            <input type="email" class="form-control" id="name" name="name" placeholder="name"
                                required>
                        </div>
                        <div class="form-group">
                            <label for="quantity">Price:</label>
                            <input type="email" class="form-control" id="price" name="price" placeholder="price"
                                required>
                        </div>
                        <div class="form-group">
                            <label for="quantity">Desctiption:</label>
                            <input type="email" class="form-control" id="description" name="description" placeholder="description"
                                required>
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection